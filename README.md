# Calculator Wall Paint

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Use:

```
git@gitlab.com:thaismts/test-digital-house.git

cd test-digital-house

npm install

npm start

```
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
