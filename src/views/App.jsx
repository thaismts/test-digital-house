import React from "react";
import Form from "./../components/Form"

const App = () => {
    return (
        <div className="container">
            <h1 className="title">Calculator Wall Paint</h1>
            <Form />
        </div>
    )
}

export default App