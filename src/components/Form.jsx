import React, { useState } from "react";
import { getAmountOfPaintCans } from "../utils";
import Button from "./Button";
import Input from "./Input";

const Form = () => {
  const [walls, setWalls] = useState([]);
  const [data, setData] = useState({
    heigth: "",
    width: "",
    doors: "",
    windows: "",
  });
  const [result, setResult] = useState([]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  function add() {
    if (walls.length === 4) {
      return;
    }
    setWalls([...walls, data]);
  }

  function calc() {
    if (result.length > 0) {
      return;
    }
    setResult(getAmountOfPaintCans(walls));
    document.getElementById("result-title").style.display = "block"
  }

  function reset() {
    window.location.reload();
  }

  return (
    <div className="box">
      <div className="form">
        <form>
          <table className="table-form">
            <tbody>
              <tr>
                <td>
                  <label>Altura</label>
                  <Input
                    inputValue={data.heigth}
                    inputOnchange={handleChange}
                    inputName="heigth"
                    inputPlaceholder="Ex. 1.5"
                  />
                </td>
                <td>
                  <label>Largura</label>
                  <Input
                    inputValue={data.width}
                    inputOnchange={handleChange}
                    inputName="width"
                    inputPlaceholder="Ex. 3"
                  />
                </td>
                <td>
                  <label>Portas</label>
                  <Input
                    inputValue={data.doors}
                    inputOnchange={handleChange}
                    inputName="doors"
                    inputPlaceholder="Ex. 2"
                  />
                </td>
                <td>
                  <label>Janelas</label>
                  <Input
                    inputValue={data.windows}
                    inputOnchange={handleChange}
                    inputName="windows"
                    inputPlaceholder="Ex. 1"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </form>
        <div>
          {walls.length > 0 &&
            walls.map((wall, i) => (
              <div key={i} className="walls-text">
                <div>
                  Parede {i + 1} Altura: {wall.heigth} Largura: {wall.width} Quantidade Portas: {wall.doors} Quantidade
                  janelas: {wall.windows}
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="result">
        <div className="walls-text" id="result-title" style={{display: "none"}}>Será necessário:</div>
        {result.length > 0 &&
          result.map((wall, i) => (
            <div key={i} className="walls-text">
              <span>
                {wall.quantity} latas de {wall.size}L
              </span>
            </div>
          ))}
      </div>
      <div className="container-btn-calcular">
        <Button btnTitle="Adicionar" btnFunction={add} />
        <Button btnTitle="Calcular" btnFunction={calc} />
        <Button btnTitle="Limpar Tela" btnFunction={reset} />
      </div>
    </div>
  );
};

export default Form;
