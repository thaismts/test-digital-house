import React from "react";

const Input = (props) => {
  return (
    <input
      type="text"
      placeholder={props.inputPlaceholder}
      value={props.inputValue}
      onChange={props.inputOnchange}
      name={props.inputName}
    />
  );
};

export default Input;
