import React from "react";

const Button = (props) => {
    return (
        <button className="btn-calcular" onClick={props.btnFunction}>
          {props.btnTitle}
        </button>
    )
}

export default Button