const AREA_DOOR = 1.52
const AREA_WINDOW = 2.4
const SIZE_CANS = [18, 3.6, 2.5, 0.5]
const LIST = []


/**
 * Get area total of walls. 
 * 
 * @param {object} walls 
 * @returns {number}
 */
function getAereaWalls (walls) {
    return walls.reduce((total, currentValue) => { 
        return total+= Number(currentValue.heigth) * Number(currentValue.width)
    }, 0)
}

/**
 * Get area total of doors.
 * 
 * @param {object} walls 
 * @returns {number}
 */
function getAreaDoors (walls) {
    return walls.reduce((total, currentValue) => { 
        return total+= Number(currentValue.doors) * AREA_DOOR
    }, 0)
}

/**
 * Get area total of windows.
 * 
 * @param {object} walls 
 * @returns {number}
 */
function getAreaWindows (walls) {
    return walls.reduce((total, currentValue) => { 
        return total+= Number(currentValue.windows) * AREA_WINDOW
    }, 0)
}

/**
 * Mouted List.
 * 
 * @param {object} totalLitersOfPaint 
 * @returns {number}
 */
function mountedListOfPaintCans (totalLitersOfPaint) {
    const canOfPaint = SIZE_CANS.find(lata => lata <= totalLitersOfPaint )
    totalLitersOfPaint -= canOfPaint
    if (totalLitersOfPaint > 0 ){
        LIST.push({size: canOfPaint})
        return mountedListOfPaintCans(totalLitersOfPaint) 
    };
    return LIST.reduce((acc, cur) => {
        const key = Object.keys(cur)[0]
        const oldIndex = acc.length -1 
        if(acc.length > 0 && acc[oldIndex][key] === cur[key]) {
            acc[oldIndex]['quantity']++
            return acc
        }
        
        return [
            ...acc,
            { [key]: cur[key], quantity: 1}
        ]
    }, [])
}

/**
 * Returns the total number of paint cans required
 * 
 * @param {object} walls 
 * @returns {object}
 */
export function getAmountOfPaintCans (walls) {
    const areaWalls = getAereaWalls(walls)
    const areaDoors = getAreaDoors(walls)
    const areaWindows = getAreaWindows(walls)
    const paintedArea = areaWalls - areaDoors - areaWindows
    let totalLitersOfPaint = paintedArea / 5
    return mountedListOfPaintCans(totalLitersOfPaint)
}